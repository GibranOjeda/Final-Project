/**
 * Clase ComprarBoletos, como atributo el numero de boletos y un método que decide si necesitan un guía
 * @author () 
 * @version ()
 */
public class ComprarBoletos{
    private int nBoletos;
    public ComprarBoletos(int nB){
        setNBoletos(nB);
    }

    public void setNBoletos(int nB){
            nBoletos= (nB>0)?nB:0;
    }

    public int getNBoletos(){
        return nBoletos;
    }

    public String necesitanGuia(){
        String message= (nBoletos>=4)?"Necesitan un guía":"Adelante";
        return message;
    }
}
