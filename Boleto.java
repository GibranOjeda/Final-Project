import java.text.DateFormat; 
import java.util.Date;
/**
 * Clase boleto, usada para generar boletos a los turistas, con atributos costo,fechaYHora y folio. 
 * @author () 
 * @version ()
 */
public class Boleto{
    private int costo=50;
    private String folio;
    private String fechaYHora;
    public Boleto(String fol){
        folio=fol;
        setFechaYHora();
    }
    public int getCosto(){
        return costo;
    }
    public void setCosto(int v){
        costo+=v;
    }
    public void setFolio(String f){
        folio=f;
    }
    public String getFolio(){
        return folio;
    }
    public void setFechaYHora(){
        Date currentDate = new Date();
        String dateToStr = DateFormat.getInstance().format(currentDate);
        fechaYHora=dateToStr;
    }
    public String getFechaYHora(){
        return fechaYHora;  
    }
    public String toStr(){
        String sb="Boleto para una persona: \n";
        sb+="Costo: ";
        sb+=getCosto();
        sb+="\n";
        sb+="Folio: ";
        sb+=getFolio();
        sb+="\n";
        sb+="Fecha y Hora: ";
        sb+=getFechaYHora();
        return sb;
    }
}