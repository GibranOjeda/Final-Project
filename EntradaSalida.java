import java.io.*;
/**
 * Esta clase es exclusiva para la escritura y lectura de archivos (Boletos y Libro De Visitantes).
 * @author () 
 * @version ()
 */
public class EntradaSalida{
    private int counter;
    private int visitantesExtranjeros;
    private int numeroMaximoDeVisitas;
    private String boletos[];
    private String libroDV[];
    public EntradaSalida(){
        numeroMaximoDeVisitas=15;
        boletos= new String[numeroMaximoDeVisitas];
        libroDV= new String[numeroMaximoDeVisitas];
    }

    public int getCounter(){
        return counter;
    }

    public void setCounter(int c){
        counter= (c>0)?c:0;
    }

    public int getVisitantesExtranjeros(){
        return visitantesExtranjeros;
    }

    public void setVisitantesExtranjeros(){
        visitantesExtranjeros+=1;
    }

    public int getNumeroMaximoDeVisitas(){
        return numeroMaximoDeVisitas;
    }

    public void agregaBoleto(String b){
        boletos[getCounter()-1]=b;
    }

    public void agregaVisitante(String v){
        libroDV[getVisitantesExtranjeros()-1]=v;
    }

    public void leeDatos(){
        try{
            BufferedReader bufr=new BufferedReader(new FileReader("datosIniciales.txt"));
            String s;
            s=bufr.readLine();
            counter= Integer.parseInt(s);
            s=bufr.readLine();
            visitantesExtranjeros=Integer.parseInt(s);
            bufr.close();
            BufferedReader br=new BufferedReader(new FileReader("boletos.txt")); 
            BufferedReader brr=new BufferedReader(new FileReader("libroDeVisitantes.txt"));
            for(int f=0; f<counter; f++){
                boletos[f]=br.readLine();
            }
            br.close();
            for(int j=0; j<visitantesExtranjeros;j++){
                libroDV[j]=brr.readLine();
            }
            brr.close();
        }
        catch(NumberFormatException f){
            counter=0;
            visitantesExtranjeros=0;
        }
        catch(FileNotFoundException f){
        }
        catch(IOException ie){
        }  
    }

    public void guardaDatos(){
        try{
            FileOutputStream flujoArchivoSalida= new FileOutputStream(new File("datosIniciales.txt"));
            PrintWriter fS = new PrintWriter(flujoArchivoSalida);
            fS.println(counter);
            fS.println(visitantesExtranjeros);
            fS.close();
            BufferedWriter buff = new BufferedWriter(new FileWriter("libroDeVisitantes.txt"));
            for(int t=0; t<visitantesExtranjeros;t++){
                buff.write(libroDV[t]);
                buff.newLine();
            }
            buff.close();
            BufferedWriter buffer = new BufferedWriter(new FileWriter("boletos.txt")); 
            for(int y=0; y<counter; y++){
                buffer.write(boletos[y]);
                buffer.newLine();
            }
            buffer.close();
        }
        catch(FileNotFoundException f){
        }
        catch(IOException ie){
        }  
    }
}