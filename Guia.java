/**
 * Clase guía, como atributo el número de visitantes
 * @author () 
 * @version ()
 */
public class Guia{
    private int nVisitantesAsignados;
    public Guia(int nVA){
    	setNVisitantesAsignados(nVA);
    }
    public void setNVisitantesAsignados(int nVA){
    	nVisitantesAsignados= (nVA>4)?nVA:0;
    }
    public int getNVisitantesAsignados(){
    	return nVisitantesAsignados;
    }
}
