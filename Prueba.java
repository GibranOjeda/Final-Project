import java.io.*;
import java.util.*;
import javax.swing.*;
/**
 * Write a description of class Prueba here.
 * 
 * @author () 
 * @version ()
 */
public class Prueba{
    public static void main(String[] args)throws IOException,FileNotFoundException{
        while(true){
            EntradaSalida eS= new EntradaSalida();
            eS.leeDatos();
            //Variables auxiliares
            Visitante v;
            LibroDeVisitantes lDV;
            ComprarBoletos cB;
            Boleto b;
            Guia g;
            while(eS.getCounter()<eS.getNumeroMaximoDeVisitas()){
                try{
                    String res=JOptionPane.showInputDialog("Bienvenido al centro turístico de Monte Albán, si desea comprar boletos para entrar escriba \"Si\", en caso contrario \"No\".");
                    if(res.equalsIgnoreCase("Si") || res.equalsIgnoreCase("Sí")){
                        int nBol=0;
                        boolean ver=false;
                        do{
                            try{
                                nBol=Integer.parseInt(JOptionPane.showInputDialog("Introduzca el numero de boletos que desea"));
                                ver=true;
                            }
                            catch(NumberFormatException s){
                            }
                        }
                        while(ver!=true);
                        int restantes=eS.getNumeroMaximoDeVisitas()-eS.getCounter();
                        if(nBol==0){
                            eS.guardaDatos();
                            break;
                        }
                        else if(nBol<=restantes){
                            cB= new ComprarBoletos(nBol);
                            JOptionPane.showMessageDialog(null,cB.necesitanGuia() + "\nAhora pediremos los nombres y nacionalidades para los boletos");
                            int costoPorPersona;
                            if(cB.necesitanGuia().equals("Necesitan un guía")){
                                g= new Guia(nBol);
                                costoPorPersona=100;
                            }
                            else{
                                costoPorPersona=0;
                            }
                            String name,nacionalidad;
                            for(int c=0; c<nBol; c++){
                                name= JOptionPane.showInputDialog("Nombre completo: ");
                                nacionalidad= JOptionPane.showInputDialog("¿De que país provienes?");
                                v= new Visitante(name,nacionalidad);
                                char n=v.getNombreCompleto().charAt(0);
                                char w=v.getNacionalidad().charAt(0);
                                eS.setCounter(eS.getCounter()+1);
                                String folio=n+""+w+"-"+eS.getCounter();
                                b = new Boleto(folio);
                                b.setCosto(costoPorPersona);
                                eS.agregaBoleto("Boleto con folio "+b.getFolio()+". Costo: $"+b.getCosto()+"MNX expedido el "+ b.getFechaYHora());
                                JOptionPane.showMessageDialog(null,b.toStr());
                                if(v.getNacionalidad().equalsIgnoreCase("Mexico") || v.getNacionalidad().equalsIgnoreCase("México")){
                                    //No hace nada
                                }
                                else{
                                    lDV= new LibroDeVisitantes(v.getNombreCompleto(),v.getNacionalidad(),b.getFechaYHora());
                                    eS.setVisitantesExtranjeros();
                                    eS.agregaVisitante(lDV.toStr());
                                }
                            }
                            eS.guardaDatos();
                        }
                        else if(restantes<nBol){
                            JOptionPane.showMessageDialog(null,"¡¡Lo sentimos!! Solo nos quedan "+restantes+" boletos el día de hoy.");
                        }
                    }
                    else if(res.equalsIgnoreCase("No")){
                        eS.guardaDatos();
                        break;
                    }
                }
                catch(NullPointerException k){
                }
            }
            if(eS.getCounter()==eS.getNumeroMaximoDeVisitas()){
                int r=0;
                try{
                    r=Integer.parseInt(JOptionPane.showInputDialog("¡¡Número máximo de visitantes!! \n¿Desea reiniciar el programa?\n Si su respuesta es afirmativa teclee \"1\" \nEn caso contrario presione \"2\""));
                }
                catch(NullPointerException o){
                }
                catch(NumberFormatException l){
                }
                if(r==1){
                    File hR= new File("datosIniciales.txt");
                    File fL= new File("boletos.txt"); 
                    File fG= new File("libroDeVisitantes.txt"); 
                    hR.delete();
                    fL.delete();
                    fG.delete();
                    JOptionPane.showInputDialog("Se ha borrado el registro anterior");
                }
                else if(r!=1){
                    eS.guardaDatos();
                    break;
                }
            }
            else{
                break;
            }
        }
    }
}