/**
 * Clase Visitante, ocupada para poder conocer el nombre y nacionalidad de nuestros visitantes.
 * @author () 
 * @version ()
 */
public class Visitante{
    private String nombreCompleto;
    private String nacionalidad;
    public Visitante(String completeName,String comeFrom){
        nombreCompleto=completeName;
        nacionalidad=comeFrom;
    }

    public void setNombreCompleto(String completeName){
        nombreCompleto=completeName;
    }

    public String getNombreCompleto(){
        return nombreCompleto;
    }

    public void setNacionalidad(String comeFrom){
        nacionalidad=comeFrom;
    }

    public String getNacionalidad(){
        return  nacionalidad;
    }
    public String toStr(){
    	String s=getNombreCompleto()+"es de "+getNacionalidad();
    	return s;
    }
}