import java.io.*;
/**
 * Clase LibroDeVisitantes, como en un libro de registro; nombre, lugar De Procedencia y fecha de entrada.
 * @author () 
 * @version ()
 */
public class LibroDeVisitantes implements Serializable{
    private String nombreCompleto;
    private String nacionalidad;
    private String fechaDEntrada;
    public LibroDeVisitantes(String nC, String n, String fDE){
    	nombreCompleto=nC;
    	nacionalidad=n;
    	fechaDEntrada=fDE;
    }
    public void setNombreCompleto(String nC){
    	nombreCompleto=nC;
    }
    public String getNombreCompleto(){
    	return nombreCompleto;
    }
    public void setNacionalidad(String n){
    	nacionalidad=n;
    }
    public String getNacionalidad(){
    	return nacionalidad;
    }
    public void setFechaDEntradas(String fDE){
    	fechaDEntrada=fDE;
    }
    public String getFechaDEntrada(){
    	return fechaDEntrada;
    }
    public String toStr(){
    	String sb="";
    	sb+=getNombreCompleto();
    	sb+="                 ";
    	sb+=getNacionalidad();
    	sb+="               ";
    	sb+=getFechaDEntrada();
    	return sb;
    }
}
